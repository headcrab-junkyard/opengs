#pragma once

class Sys final
{
public:
	static bool GetExecutableName(const char *sFileName, size_t nSize);

	static bool FileExists(const char *sFileName);

	/**
	*	Gets the directory that this executable is running from
	*/
	static const char *GetBaseDir();
};