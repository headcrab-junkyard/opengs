#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <direct.h>
#include <io.h>
#include <process.h>

#include <WinSock2.h>
#endif