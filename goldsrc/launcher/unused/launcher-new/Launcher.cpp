#include "Launcher.hpp"
#include "public/FileSystem.h"
#include "public/engine_launcher_api.h"
#include "ICommandLine.h"

void CLauncher::Run()
{
	// TODO: Could be the CRT heap init, but why is this here? - Solokiller
	//sub_14032FD(0);

	static char szNewCommandParams[2048]{}; // sPostRestartCmdLineArgs

	auto bRestart{ false }; // bRestartEngine

	PreInit();

	do
	{
		EngineRunResult runResult{ ENGRUN_QUITTING };

		bRestart = false;
		szNewCommandParams[0] = '\0';

		if(!Init())
			return;

		runResult = pEngine->Run(nullptr,
		                         "",
		                         "",
		                         Sys_GetFactoryThis(),
		                         fnFileSystemFactory) == ENGINE_RESULT_RESTART;

		switch(runResult)
		{
		case ENGRUN_QUITTING:
			bRestart = false;
			break;
		case ENGRUN_CHANGED_VIDEOMODE:
			bRestart = true;
			break;
		case ENGRUN_UNSUPPORTED_VIDEOMODE:
			bRestart = OnVideoModeFailed();
			break;
		default:
			break;
		};

		// If we're restarting, remove any parameters that could affect video mode changes.
		// Also remove parameters that trigger events automatically, such as connecting to a server.
		cmdline->RemoveParm("-sw");
		cmdline->RemoveParm("-startwindowed");
		cmdline->RemoveParm("-windowed");
		cmdline->RemoveParm("-window");
		cmdline->RemoveParm("-full");
		cmdline->RemoveParm("-fullscreen");
		cmdline->RemoveParm("-soft");
		cmdline->RemoveParm("-software");
		cmdline->RemoveParm("-gl");
		cmdline->RemoveParm("-d3d");
		cmdline->RemoveParm("-w");
		cmdline->RemoveParm("-width");
		cmdline->RemoveParm("-h");
		cmdline->RemoveParm("-height");
		cmdline->RemoveParm("+connect");

		cmdline->SetParm("-novid", nullptr);

		// User changed game
		if(strstr(szNewCommandParams, "-game"))
			cmdline->RemoveParm("-game");

		// Remove saved game load command if new command is present
		if(strstr(szNewCommandParams, "+load"))
			cmdline->RemoveParm("+load");

		// Append new command line to process properly
		cmdline->AppendParm(szNewCommandParams, nullptr);

		m_pFileSystem->Unmount();
		Sys_UnloadModule(hFSModule);
	} while(bRestart);

	Shutdown();
};

bool CLauncher::Init()
{
	char FileName[256]{};
	Sys::GetExecutableName(FileName, sizeof(FileName));

	// Load and mount the filesystem
	auto hFSModule{ LoadFileSystemModule(FileName, cmdline->CheckParm("-game", nullptr) != nullptr) };

	if(!hFSModule)
		break;

	auto fnFileSystemFactory{ Sys_GetFactory(hFSModule) };
	m_pFileSystem = static_cast<IFileSystem *>(fnFileSystemFactory(FILESYSTEM_INTERFACE_VERSION, nullptr));

	m_pFileSystem->Mount();

	m_pFileSystem->AddSearchPath(UTIL_GetBaseDir(), "ROOT");

	//const char *sEngDLL{CommandLine()->GetParam("-engdll")};

	//if(!sEngDLL || !*sEngDLL)
	//sEngDLL = "engine";

	auto pEngineModule{ Sys_LoadModule("engine") };

	if(pEngineModule)
		return false;

	auto fnEngineFactory{ Sys_GetFactory(pEngineModule) };

	if(!fnEngineFactory)
		return false;

	auto pEngine{ static_cast<IEngineAPI *>(fnEngineFactory(VENGINE_LAUNCHER_API_VERSION, nullptr)) };

	if(!pEngine)
		return false;

	return true;
};

void CLauncher::SetEngineDLL(const char **ppEngineDLL)
{
	*ppEngineDLL = "hw.dll";

	const char *pEngineDLLSetting = registry->ReadString("EngineDLL", "hw.dll");
	if(_stricmp(pEngineDLLSetting, "hw.dll"))
	{
		if(!_stricmp(pEngineDLLSetting, "sw.dll"))
			*ppEngineDLL = "sw.dll";
	}
	else
		*ppEngineDLL = "hw.dll";

	if(cmdline->CheckParm("-soft", nullptr) || cmdline->CheckParm("-software", nullptr))
	{
		*ppEngineDLL = "sw.dll";
	}
	else if(cmdline->CheckParm("-gl", nullptr) || cmdline->CheckParm("-d3d", nullptr))
	{
		*ppEngineDLL = "hw.dll";
	};

	registry->WriteString("EngineDLL", *ppEngineDLL);
};

CSysModule *CLauncher::LoadFileSystemModule(const char *exename, bool bRunningSteam)
{
	auto pModule{ Sys_LoadModule(filepath::FILESYSTEM_STDIO) };

	if(!pModule)
	{
		if(strchr(exename, ';'))
		{
			MessageBoxA(nullptr, "Game cannot be run from directories containing the semicolon char", "Fatal Error", MB_ICONERROR);
			return nullptr;
		};

		struct _finddata_t find_data;

		auto result = _findfirst(filepath::FILESYSTEM_STDIO, &find_data);

		if(result == -1)
			MessageBoxA(nullptr, "Could not find filesystem dll to load.", "Fatal Error", MB_ICONERROR);
		else
		{
			MessageBoxA(nullptr, "Could not load filesystem dll.", "Fatal Error", MB_ICONERROR);
			_findclose(result);
		};
	};

	return pModule;
};