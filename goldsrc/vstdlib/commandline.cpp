/*
 *	This file is part of OGS Engine
 *	Copyright (C) 2016-2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include "commandline.h"

static const int MAX_PARAMETER_LEN = 128;

//CCommandLine g_CmdLine;
ICommandLine *g_pCmdLine = nullptr; //(ICommandLine *)&g_CmdLine;

ICommandLine *CommandLine()
{
	return g_pCmdLine;
}

/*
CCommandLine::CCommandLine()
{
	m_pszCmdLine = 0;
}

CCommandLine::~CCommandLine()
{
	delete[] m_pszCmdLine;
}

void CCommandLine::LoadParametersFromFile(char *&pSrc, char *&pDst)
{
	char szFileName[_MAX_PATH];
	char *pOut;

	pSrc++;
	pOut = szFileName;

	while(*pSrc && *pSrc != ' ')
		*pOut++ = *pSrc++;

	*pOut = '\0';

	if(*pSrc)
		pSrc++;

	FILE *fp = fopen(szFileName, "r");

	if(fp)
	{
		char c;
		c = (char)fgetc(fp);

		while(c != EOF)
		{
			if(c == '\n')
				c = ' ';

			*pDst++ = c;
			c = (char)fgetc(fp);
		}

		*pDst++ = ' ';
		fclose(fp);
	}
	else
		printf("Parameter file '%s' not found, skipping...", szFileName);
}

void CCommandLine::SetParm(const char *pszParm, const char *pszValues)
{
	RemoveParm(pszParm);
	AppendParm(pszParm, pszValues);
}

const char *CCommandLine::GetCmdLine() const
{
	return m_pszCmdLine;
}

const char *CCommandLine::CheckParm(const char *psz, const char **ppszValue) const
{
	static char sz[MAX_PARAMETER_LEN];
	char *pret;

	if(!m_pszCmdLine)
		return NULL;

	pret = strstr(m_pszCmdLine, psz);

	if(pret && ppszValue)
	{
		char *p1 = pret;
		*ppszValue = 0;

		while(*p1 && (*p1 != 32))
			p1++;

		char *p2 = ++p1;
		int i;

		for(i = 0; i < MAX_PARAMETER_LEN; i++)
		{
			if(!*p2 || (*p2 == 32))
				break;

			sz[i] = *p2++;
		}

		sz[i] = 0;
		*ppszValue = &sz[0];
	}

	return pret;
}

void CCommandLine::SetParm(const char *pszParm, int iValue)
{
	char pszValue[64];
	_snprintf(pszValue, sizeof(pszValue), "%d", iValue);
	SetParm(pszParm, iValue);
}

void CCommandLine::AppendParm(const char *pszParm, const char *pszValues)
{
	int nNewLength = 0;
	char *pCmdString;

	nNewLength = strlen(pszParm);

	if(pszValues)
		nNewLength += strlen(pszValues) + 1;

	if(!m_pszCmdLine)
	{
		m_pszCmdLine = new char[nNewLength];
		strcpy(m_pszCmdLine, pszParm);

		if(pszValues)
		{
			strcat(m_pszCmdLine, " ");
			strcat(m_pszCmdLine, pszValues);
		}

		return;
	}

	RemoveParm(pszParm);

	nNewLength += strlen(m_pszCmdLine) + 1 + 1;
	pCmdString = new char[nNewLength];
	memset(pCmdString, 0, nNewLength);

	strcpy(pCmdString, m_pszCmdLine);
	strcat(pCmdString, " ");
	strcat(pCmdString, pszParm);

	if(pszValues)
	{
		strcat(pCmdString, " ");
		strcat(pCmdString, pszValues);
	}

	delete[] m_pszCmdLine;
	m_pszCmdLine = pCmdString;
}

void CCommandLine::RemoveParm(const char *pszParm)
{
	if(!m_pszCmdLine)
		return;

	if(!pszParm || !*pszParm)
		return;

	char *p, *found;
	char *pnextparam;
	int n;
	int curlen;
	int curpos;

	while(1)
	{
		p = m_pszCmdLine;
		curlen = strlen(p);
		found = strstr(p, pszParm);

		if(!found)
			break;

		curpos = found - p;
		pnextparam = found + 1;

		while(pnextparam && *pnextparam && (*pnextparam != '-') && (*pnextparam != '+'))
			pnextparam++;

		if(pnextparam && *pnextparam)
		{
			n = curlen - (pnextparam - p);

			if(found[0] == ' ')
			{
				memcpy(found + 1, pnextparam, n);
				found[n + 1] = '\0';
			}
			else
			{
				memcpy(found, pnextparam, n);
				found[n] = '\0';
			}
		}
		else
		{
			n = pnextparam - found;
			memset(found, 0, n);
		}
	}

	while(m_pszCmdLine[strlen(m_pszCmdLine) - 1] == ' ')
		m_pszCmdLine[strlen(m_pszCmdLine) - 1] = '\0';
}

void CCommandLine::CreateCmdLine(const char *commandline)
{
	if(m_pszCmdLine)
		delete[] m_pszCmdLine;

	char szFull[4096];
	int len = strlen(commandline) + 1;
	char *pOrig = new char[len];
	memcpy(pOrig, commandline, len);

	char *pSrc, *pDst;

	pDst = szFull;
	pSrc = pOrig;

	bool bContinue = true;

	while(*pSrc)
	{
		if(bContinue && *pSrc == '@')
		{
			LoadParametersFromFile(pSrc, pDst);
			continue;
		}

		bContinue = (isspace(*pSrc) != false);
		*pDst++ = *pSrc++;
	}

	*pDst = '\0';

	len = strlen(szFull) + 1;
	m_pszCmdLine = new char[len];
	memcpy(m_pszCmdLine, szFull, len);
}
*/