#include <cstring>
#include "vstdlib/strtools.h"

void Q_hextobinary(char const *in, int numchars, byte *out, int maxoutputbytes)
{
};

void Q_binarytohex(const byte *in, int inputbytes, char *out, int outsize)
{
};

int Q_strcasecmp(const char *s1, const char *s2)
{
	return Q_stricmp(s1, s2);
}

int Q_strncasecmp(const char *s1, const char *s2, int n)
{
	return Q_strnicmp(s1, s2, n);
}

int Q_NormalizeUTF8Old(const char *pchSrc, OUT_Z_CAP(cchDest) char *pchDest, int cchDest)
{
	return 0;
};

int Q_NormalizeUTF8(const char *pchSrc, OUT_Z_CAP(cchDest) char *pchDest, int cchDest)
{
	return 0;
};