/**
*	@file
*
*	Header that includes the base Windows headers
*/

#pragma once

#ifdef _WIN32
#include "common/winsani_in.h"
#include "winlite.h"
#include "common/winsani_out.h"

#undef GetCurrentTime
#endif