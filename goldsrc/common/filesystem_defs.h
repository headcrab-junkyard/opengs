/// @file

#pragma once

// NOTE: Moved here from rehlds public/FileSystem.h

// There is only one instance of the IFileSystem interface,
// located in the filesystem_stdio library (filesystem_steam is obsolete).
#ifdef _WIN32
const char STDIO_FILESYSTEM_LIB[] = "filesystem_stdio.dll";
const char STEAM_FILESYSTEM_LIB[] = "filesystem_steam.dll";
#else
const char STDIO_FILESYSTEM_LIB[] = "filesystem_stdio.so";
const char STEAM_FILESYSTEM_LIB[] = "filesystem_steam.so";
#endif // _WIN32