project(OGS-root)

# Mark this folder as root
set(OGS_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

# Set C++ code standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(OGS_HLSDK_PATH
	#${CMAKE_SOURCE_DIR}/../hlsdk-core
	${CMAKE_SOURCE_DIR}/hlsdk
	CACHE PATH "Path to the Half-Life SDK"
)

set(OGS_GOLDSRCSDK_PATH
	${CMAKE_SOURCE_DIR}/external/goldsrc-sdk
	CACHE PATH "Path to the GoldSrc SDK"
)

# Add include directories
include_directories(
	${OGS_HLSDK_PATH}
	${CMAKE_SOURCE_DIR}/external/SDL2/include
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/common
	${CMAKE_CURRENT_SOURCE_DIR}/public
	${OGS_GOLDSRCSDK_PATH}/goldsrc/common
	${OGS_GOLDSRCSDK_PATH}/goldsrc/public
	${OGS_GOLDSRCSDK_PATH}/goldsrc/utils
	${OGS_GOLDSRCSDK_PATH}/goldsrc/public/tier0
	${OGS_GOLDSRCSDK_PATH}/goldsrc/public/tier1
)

# Add external subdirectories
add_subdirectory(${OGS_GOLDSRCSDK_PATH} ${CMAKE_BINARY_DIR}/goldsrc-sdk)

# Add our modules

#add_subdirectory(unittests)

add_subdirectory(tier0) # unfinished
add_subdirectory(vstdlib)

add_subdirectory(common)

add_subdirectory(filesystem) # -lstdc++fs related linker error

add_subdirectory(dedicated)

add_subdirectory(optional)

#add_subdirectory(hookers)
#add_subdirectory(rehlds)
#add_subdirectory(testsuite)

add_subdirectory(engine) # unresolved
#add_subdirectory(HLTV)

# Client-side modules
if(NOT OGS_DEDICATED)
	# Add VGUI2 first since it's referenced by other projects
	add_subdirectory(vgui2)
	
	#add_subdirectory(GameUI) #unresolved
	
	add_subdirectory(launcher)
	
	#add_subdirectory(particleman) # broken
	
	if(OGS_EVOL)
		#add_subdirectory(render) # broken
	endif()
	
	#add_subdirectory(voice) # Where's it now?
endif()