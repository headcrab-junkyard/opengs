/// @file

#pragma once

#include "basetypes.h"
#include "public/FileSystem.h"

#include "filesystem_defs.h"

#ifndef _vsnprintf
	#include <cstdio>
	#define _vsnprintf vsnprintf
#endif

#include "strtools_local.h"

//#include "common.h"
//#include "mem.h"

#include "engine_hlds_api.h"
#include "idedicatedexports.h"
#include "icommandline.h"

#include "isys.h"
#include "common/dll_state.h"
#include "dedicated.h"

#include "sys_ded.h"
#include "TextConsole.h"
#include "vgui/vguihelpers.h"

#include "IObjectContainer.h"
#include "ObjectList.h"

#ifdef _WIN32
#include "conproc.h"
#include <mmsystem.h> // timeGetTime
#else
#include <signal.h>
#endif // _WIN32