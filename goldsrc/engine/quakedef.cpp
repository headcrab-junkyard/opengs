/// @file
/// @brief This file creates the quakedef precompiled header

// NOTE: to change precompiled header settings for engine module,
// you need to edit the DSP file because it is setup to do multiple precompiled headers

#include "quakedef.h"