/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/**
*	@file
*
*	defs common to client and server
*/

// Primary header for engine

#pragma once

// For backwards compatibility only, SDK headers use it - Solokiller
#define QUAKEDEF_H

//unused stuff
//#define GLTEST ///< experimental stuff
//#define QUAKE_GAME ///< as opposed to utilities
//#define VERSION 0.5
//#define GAMENAME "valve" ///< directory to look in by default
//

//define PARANOID ///< speed sapping error checking

#ifdef _WIN32
#pragma warning( disable : 4244 4127 4201 4214 4514 4305 4115 4018)
#endif

#include <cmath>
#include <cstring>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <csetjmp>
#include <ctime>
#include <cctype>

#if defined(_WIN32) && !defined(WINDED)

#if defined(_M_IX86)
#define __i386__	1
#endif

void	VID_LockBuffer ();
void	VID_UnlockBuffer ();

#else

#define	VID_LockBuffer()
#define	VID_UnlockBuffer()

#endif

#if defined __i386__ // && !defined __sun__
//#if (defined(_M_IX86) || defined(__i386__)) && !defined(id386)
#define id386	1
#else
#define id386	0
#endif

#ifdef SERVERONLY		///< no asm in dedicated server
#undef id386
#endif

#if id386
#define UNALIGNED_OK	1	///< set to 0 if unaligned accesses are not supported
#else
#define UNALIGNED_OK	0
#endif

// !!! if this is changed, it must be changed in d_ifacea.h too !!!
#define CACHE_SIZE	32		///< used to align key data structures

#define UNUSED(x)	(x = x)	///< for pesky compiler / lint warnings

//#define	MINIMUM_MEMORY			0x550000 // Moved to sys.h?
//#define	MINIMUM_MEMORY_LEVELPAK	(MINIMUM_MEMORY + 0x100000)

//const int MAX_NUM_ARGVS = 50; // defined in qlimits

// up / down
#define	PITCH	0

// left / right
#define	YAW		1

// fall over
#define	ROLL	2

#include "qlimits.h"

// TODO: move to protocol?
#define	MAX_MSGLEN 4000 ///< max length of a reliable message
#define	MAX_DATAGRAM 4000 ///< max length of unreliable message
	
#define	MAX_STYLESTRING	64

#define MAX_RESOURCE_LIST	1280 // TODO: find better place

#define	SAVEGAME_COMMENT_LENGTH	39

//
// stats are integers communicated to the client by the server
//
#define	MAX_CL_STATS		32
#define	STAT_HEALTH			0
//#define	STAT_FRAGS			1
#define	STAT_WEAPON			2
#define	STAT_AMMO			3
#define	STAT_ARMOR			4
//#define	STAT_WEAPONFRAME	5
#define	STAT_SHELLS			6
#define	STAT_NAILS			7
#define	STAT_ROCKETS		8
#define	STAT_CELLS			9
#define	STAT_ACTIVEWEAPON	10
#define	STAT_TOTALSECRETS	11
#define	STAT_TOTALMONSTERS	12
#define	STAT_SECRETS		13		///< bumped on client side by svc_foundsecret
#define	STAT_MONSTERS		14		///< bumped by svc_killedmonster
#define	STAT_ITEMS			15
//define	STAT_VIEWHEIGHT		16

// stock defines

const int MAX_SCOREBOARD = 16; ///< max numbers of players

const int SOUND_CHANNELS = 8;

//
// print flags
//
#define	PRINT_LOW			0		///< pickup messages
#define	PRINT_MEDIUM		1		///< death messages
#define	PRINT_HIGH			2		///< critical messages
#define	PRINT_CHAT			3		///< chat messages

//TODO: tidy these includes - Solokiller
//#include "tier0/platform.h"
//#include "commonmacros.h"
#include "filesystem.h"
#include "common/mathlib.h"
#include "common/const.h"
#include "common/crc.h"
//#include "common/com_model.h"
//#include "strtools.h"
#include "cvar.h"
#include "cmd.h"
#include "mem.h"
#include "zone.h"
#include "common.h"
#include "console.h"
#include "net.h"
#include "vid.h"
#include "host.h"
#include "sys.h"

#include "common/entity_state.h"

#include "wad.h"
#include "decals.h"
#include "draw.h"
#include "screen.h"
#include "protocol.h"
#include "sound.h"
#include "render.h"
#include "client.h"
#include "engine/progs.h"
#include "server.h"
#include "sv_main.h"
#include "sv_log.h"
#include "sv_steam3.h"

#ifdef GLQUAKE
#include "gl_model.h"
#else
//#include "model.h"
//#include "d_iface.h" // TODO: fix
#endif

#include "input.h"
#include "keys.h"
#include "view.h"
#include "menu.h"
//#include "vgui_int.h"
#include "cdaudio.h"
#include "pmove.h"
#include "info.h"
#include "world.h"

#ifdef GLQUAKE
#include "glquake.h"
#endif

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

typedef int BOOL;

// user message
const int MAX_USER_MSG_DATA = 192;

//moved to com_model.h
//typedef struct cache_user_s
//{
//	void *data;
//} cache_user_t;

typedef int (*pfnUserMsgHook)(const char *, int, void *);