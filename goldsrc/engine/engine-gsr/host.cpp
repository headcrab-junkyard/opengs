

qboolean Host_Init(quakeparms_t *parms)
{
	if(cls.state != ca_dedicated)
	{
		byte *pPalette = COM_LoadHunkFile("gfx/palette.lmp");

		if(!pPalette)
			Sys_Error("");

		byte *pSource = pPalette;

		//Convert the palette from BGR to RGBA. TODO: these are the right formats, right? - Solokiller
		host_basepal = reinterpret_cast<unsigned short *>(Hunk_AllocName(4 * 256 * sizeof(unsigned short), "palette.lmp"));

		for(int i = 0; i < 256; ++i, pSource += 3)
		{
			host_basepal[(4 * i)] = *(pSource + 2);
			host_basepal[(4 * i) + 1] = *(pSource + 1);
			host_basepal[(4 * i) + 2] = *pSource;
			host_basepal[(4 * i) + 3] = 0;
		}
	}
}