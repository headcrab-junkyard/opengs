#pragma once

#include "wad.h"

extern float scr_fov_value;

extern int glx;
extern int gly;
extern int glwidth;
extern int glheight;

void SCR_DrawConsole();

void Draw_CenterPic(qpic_t *pPic);

void SCR_DrawLoading();
