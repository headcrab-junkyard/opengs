/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

//#include "precompiled.h"
#include "quakedef.h"
//#include "pmovetst.h"

static hull_t box_hull; // TODO: non-static?
static dclipnode_t box_clipnodes[6]; // TODO: non-static?
static mplane_t box_planes[6]; // TODO: non-static?

extern vec3_t player_mins;
extern vec3_t player_maxs;

/*
===================
PM_InitBoxHull

Set up the planes and clipnodes so that the six floats of a bounding box
can just be stored out and get a proper hull_t structure.
===================
*/
void PM_InitBoxHull()
{
	int i;
	int side;

	box_hull.clipnodes = box_clipnodes;
	box_hull.planes = box_planes;
	box_hull.firstclipnode = 0;
	box_hull.lastclipnode = 5;

	for(i = 0; i < 6; i++)
	{
		box_clipnodes[i].planenum = i;

		side = i & 1;

		box_clipnodes[i].children[side] = CONTENTS_EMPTY;
		if(i != 5)
			box_clipnodes[i].children[side ^ 1] = i + 1;
		else
			box_clipnodes[i].children[side ^ 1] = CONTENTS_SOLID;

		box_planes[i].type = i >> 1;
		box_planes[i].normal[i >> 1] = 1;
	}
}

/*
===================
PM_HullForBox

To keep everything totally uniform, bounding boxes are turned into small
BSP trees instead of being compared directly.
===================
*/
//hull_t *PM_HullForBox(vec_t *mins, vec_t *maxs)
hull_t *PM_HullForBox(vec3_t mins, vec3_t maxs)
{
	box_planes[0].dist = maxs[0];
	box_planes[1].dist = mins[0];
	box_planes[2].dist = maxs[1];
	box_planes[3].dist = mins[1];
	box_planes[4].dist = maxs[2];
	box_planes[5].dist = mins[2];

	return &box_hull;
}

/*
==================
PM_HullPointContents

==================
*/
/*EXT_FUNC*/ int PM_HullPointContents(hull_t *hull, int num, vec3_t p)
{
	float d;
	dclipnode_t *node;
	mplane_t *plane;

	while(num >= 0)
	{
		if(num < hull->firstclipnode || num > hull->lastclipnode)
			Sys_Error("PM_HullPointContents: bad node number");

		node = hull->clipnodes + num;
		plane = hull->planes + node->planenum;

		if(plane->type < 3)
			d = p[plane->type] - plane->dist;
		else
			d = DotProduct(plane->normal, p) - plane->dist;
		if(d < 0)
			num = node->children[1];
		else
			num = node->children[0];
	}

	return num;
};

int PM_LinkContents(vec_t *p, int *pIndex)
{
	physent_t *pe;
	vec3_t test;

	for (int i = 1; i < pmove->numphysent; i++) {
		pe = &pmove->physents[i];
		model_t* model = pmove->physents[i].model;
		if (pmove->physents[i].solid || model == NULL)
			continue;

		test[0] = p[0] - pe->origin[0];
		test[1] = p[1] - pe->origin[1];
		test[2] = p[2] - pe->origin[2];
		if (PM_HullPointContents(model->hulls, model->hulls[0].firstclipnode, test) != -1) {
			if (pIndex)
				*pIndex = pe->info;
			return pe->skin;
		}
	}

	return -1;
};

/*
==================
PM_PointContents

==================
*/
//int PM_PointContents( vec_t* p, int* truecontents )
/*EXT_FUNC*/ int PM_PointContents(vec3_t p, int *truecontents)
{
	float d;
	dclipnode_t *node;
	mplane_t *plane;

#ifndef SWDS
	g_engdstAddrs.PM_PointContents(&p, &truecontents);
#endif

	hull_t *hull = &pmove.physents[0].model->hulls[0];

	int num = hull->firstclipnode;

	while(num >= 0)
	{
		if(num < hull->firstclipnode || num > hull->lastclipnode)
			Sys_Error("PM_HullPointContents: bad node number");

		node = hull->clipnodes + num;
		plane = hull->planes + node->planenum;

		if(plane->type < 3)
			d = p[plane->type] - plane->dist;
		else
			d = DotProduct(plane->normal, p) - plane->dist;
		if(d < 0)
			num = node->children[1];
		else
			num = node->children[0];
	}

	return num;
}

int EXT_FUNC PM_TruePointContents(vec_t *p)
{
	if ((int)pmove->physents[0].model == -208)
		return -1;
	else
		return PM_HullPointContents(pmove->physents[0].model->hulls, pmove->physents[0].model->hulls[0].firstclipnode, p);
}

int PM_WaterEntity(vec_t *p)
{
	int cont;
	int entityIndex;

#ifndef SWDS
	g_engdstAddrs.PM_WaterEntity(&p);
#endif

	model_t* model = pmove->physents[0].model;
	cont = PM_HullPointContents(model->hulls, model->hulls[0].firstclipnode, p);
	if (cont == -2) {
		return -1;
	}

	entityIndex = 0;
	return PM_LinkContents(p, &entityIndex);
};

hull_t *PM_HullForStudioModel(model_t *pModel, vec_t *offset, float frame, int sequence, const vec_t *angles, const vec_t *origin, const unsigned char *pcontroller, const unsigned char *pblending, int *pNumHulls)
{
	vec3_t size;

	size[0] = player_maxs[pmove->usehull][0] - player_mins[pmove->usehull][0];
	size[1] = player_maxs[pmove->usehull][1] - player_mins[pmove->usehull][1];
	size[2] = player_maxs[pmove->usehull][2] - player_mins[pmove->usehull][2];
	VectorScale(size, 0.5, size);
	offset[0] = 0;
	offset[1] = 0;
	offset[2] = 0;
	return R_StudioHull(pModel, frame, sequence, angles, origin, size, pcontroller, pblending, pNumHulls, 0, 0);
}

hull_t* EXT_FUNC PM_HullForBsp(physent_t *pe, vec_t *offset)
{
	hull_t *hull;

	switch (pmove->usehull) {
	case 1:
		hull = &pe->model->hulls[3];
		break;

	case 2:
		hull = &pe->model->hulls[0];
		break;

	case 3:
		hull = &pe->model->hulls[2];
		break;

	default:
		hull = &pe->model->hulls[1];
		break;
	}

	offset[0] = hull->clip_mins[0] - player_mins[pmove->usehull][0];
	offset[1] = hull->clip_mins[1] - player_mins[pmove->usehull][1];
	offset[2] = hull->clip_mins[2] - player_mins[pmove->usehull][2];
	offset[0] += pe->origin[0];
	offset[1] += pe->origin[1];
	offset[2] += pe->origin[2];
	return hull;
}

/*
===============================================================================

LINE TESTING IN HULLS

===============================================================================
*/

/*
================
PM_TestPlayerPosition

Returns false if the given player position is not valid (in solid)
================
*/
qboolean _PM_TestPlayerPosition(vec3_t pos, pmtrace_t *ptrace, int(*pfnIgnore)(physent_t *))
{
	int i;
	physent_t *pe;
	vec3_t mins, maxs, test;
	hull_t *hull;

	for(i = 0; i < pmove.numphysent; i++)
	{
		pe = &pmove.physents[i];
		// get the clipping hull
		if(pe->model)
			hull = &pmove.physents[i].model->hulls[1];
		else
		{
			VectorSubtract(pe->mins, player_maxs, mins);
			VectorSubtract(pe->maxs, player_mins, maxs);
			hull = PM_HullForBox(mins, maxs);
		}

		VectorSubtract(pos, pe->origin, test);

		if(PM_HullPointContents(hull, hull->firstclipnode, test) == CONTENTS_SOLID)
			return false;
	}

	return true;
}

/*EXT_FUNC*/ int PM_TestPlayerPosition(vec_t *pos, pmtrace_t *ptrace)
{
	return _PM_TestPlayerPosition(pos, ptrace, 0);
}

/*EXT_FUNC*/ int PM_TestPlayerPositionEx(vec_t *pos, pmtrace_t *ptrace, int(*pfnIgnore)(physent_t *))
{
	return _PM_TestPlayerPosition(pos, ptrace, pfnIgnore);
}

/*
================
PM_PlayerMove
================
*/
/*
pmtrace_t PM_PlayerMove(vec3_t start, vec3_t end)
{
	pmtrace_t trace, total;
	vec3_t offset;
	vec3_t start_l, end_l;
	hull_t *hull;
	int i;
	physent_t *pe;
	vec3_t mins, maxs;

	// fill in a default trace
	memset(&total, 0, sizeof(pmtrace_t));
	total.fraction = 1;
	total.ent = -1;
	VectorCopy(end, total.endpos);

	for(i = 0; i < pmove.numphysent; i++)
	{
		pe = &pmove.physents[i];
		// get the clipping hull
		if(pe->model)
			hull = &pmove.physents[i].model->hulls[1];
		else
		{
			VectorSubtract(pe->mins, player_maxs, mins);
			VectorSubtract(pe->maxs, player_mins, maxs);
			hull = PM_HullForBox(mins, maxs);
		}

		// PM_HullForEntity (ent, mins, maxs, offset);
		VectorCopy(pe->origin, offset);

		VectorSubtract(start, offset, start_l);
		VectorSubtract(end, offset, end_l);

		// fill in a default trace
		memset(&trace, 0, sizeof(pmtrace_t));
		trace.fraction = 1;
		trace.allsolid = true;
		//		trace.startsolid = true;
		VectorCopy(end, trace.endpos);

		// trace a line through the apropriate clipping hull
		PM_RecursiveHullCheck(hull, hull->firstclipnode, 0, 1, start_l, end_l, &trace);

		if(trace.allsolid)
			trace.startsolid = true;
		if(trace.startsolid)
			trace.fraction = 0;

		// did we clip the move?
		if(trace.fraction < total.fraction)
		{
			// fix trace up by the offset
			VectorAdd(trace.endpos, offset, trace.endpos);
			total = trace;
			total.ent = i;
		}
	}

	return total;
}
*/

pmtrace_t _PM_PlayerTrace(vec_t *start, vec_t *end, int traceFlags, int numphysent, physent_t *physents, int ignore_pe, int(*pfnIgnore)(physent_t *))
{
	hull_t *hull;
	pmtrace_t trace;
	pmtrace_t testtrace;
	pmtrace_t total;
	vec3_t maxs;
	vec3_t mins;
	int closest;
	bool rotated;
	int pNumHulls;
	vec_t end_l[3];
	vec_t start_l[3];
	vec3_t offset;

	Q_memset(&trace, 0, sizeof(trace));
	trace.fraction = 1.0f;
	trace.ent = -1;
	trace.endpos[0] = end[0];
	trace.endpos[1] = end[1];
	trace.endpos[2] = end[2];

	for (int i = 0; i < numphysent; i++)
	{
		physent_t* pe = &physents[i];
		if (i > 0 && (traceFlags & PM_WORLD_ONLY))
			break;

		if (pfnIgnore)
		{
			if (pfnIgnore(pe))
				continue;
		}
		else
		{
			if (ignore_pe != -1 && i == ignore_pe)
				continue;
		}

		if ((pe->model && !pe->solid && pe->skin) || ((traceFlags & PM_GLASS_IGNORE) && pe->rendermode))
			continue;


		offset[0] = pe->origin[0];
		offset[1] = pe->origin[1];
		offset[2] = pe->origin[2];
		pNumHulls = 1;
		if (pe->model)
		{
			switch (pmove->usehull)
			{
			case 1:
				hull = &pe->model->hulls[3];
				break;
			case 2:
				hull = &pe->model->hulls[0];
				break;
			case 3:
				hull = &pe->model->hulls[2];
				break;
			default:
				hull = &pe->model->hulls[1];
				break;
			}
			offset[0] = hull->clip_mins[0] - player_mins[pmove->usehull][0];
			offset[1] = hull->clip_mins[1] - player_mins[pmove->usehull][1];
			offset[2] = hull->clip_mins[2] - player_mins[pmove->usehull][2];
			offset[0] = offset[0] + pe->origin[0];
			offset[1] = offset[1] + pe->origin[1];
			offset[2] = offset[2] + pe->origin[2];
		}
		else
		{
			hull = NULL;
			if (pe->studiomodel)
			{
				if (traceFlags & PM_STUDIO_IGNORE)
					continue;


				if (pe->studiomodel->type == mod_studio && (pe->studiomodel->flags & STUDIO_TRACE_HITBOX || (pmove->usehull == 2 && !(traceFlags & PM_STUDIO_BOX))))
				{
					hull = PM_HullForStudioModel(pe->studiomodel, offset, pe->frame, pe->sequence, pe->angles, pe->origin, pe->controller, pe->blending, &pNumHulls);
				}
			}

			if (!hull)
			{
				mins[0] = pe->mins[0] - player_maxs[pmove->usehull][0];
				mins[1] = pe->mins[1] - player_maxs[pmove->usehull][1];
				mins[2] = pe->mins[2] - player_maxs[pmove->usehull][2];
				maxs[0] = pe->maxs[0] - player_mins[pmove->usehull][0];
				maxs[1] = pe->maxs[1] - player_mins[pmove->usehull][1];
				maxs[2] = pe->maxs[2] - player_mins[pmove->usehull][2];
				hull = PM_HullForBox(mins, maxs);
			}
		}

		start_l[0] = start[0] - offset[0];
		start_l[1] = start[1] - offset[1];
		start_l[2] = start[2] - offset[2];
		end_l[0] = end[0] - offset[0];
		end_l[1] = end[1] - offset[1];
		end_l[2] = end[2] - offset[2];

		if (pe->solid == SOLID_BSP && (pe->angles[0] != 0.0 || pe->angles[1] != 0.0 || pe->angles[2] != 0.0))
		{
			vec3_t forward, right, up;
			AngleVectors(pe->angles, forward, right, up);

			vec3_t temp_start = {start_l[0], start_l[1], start_l[2]};
			start_l[0] = _DotProduct(forward, temp_start);
			start_l[1] = -_DotProduct(right, temp_start);
			start_l[2] = _DotProduct(up, temp_start);

			vec3_t temp_end = {end_l[0], end_l[1], end_l[2]};
			end_l[0] = _DotProduct(forward, temp_end);
			end_l[1] = -_DotProduct(right, temp_end);
			end_l[2] = _DotProduct(up, temp_end);

			rotated = true;
		}
		else
		{
			rotated = false;
		}

		Q_memset(&total, 0, sizeof(total));
		total.endpos[0] = end[0];
		total.endpos[1] = end[1];
		total.endpos[2] = end[2];
		total.fraction = 1.0f;
		total.allsolid = 1;
		if (pNumHulls <= 0)
		{
			total.allsolid = 0;
		}
		else
		{
			if (pNumHulls == 1)
			{
				PM_RecursiveHullCheck(hull, hull->firstclipnode, 0.0, 1.0, start_l, end_l, &total);
			}
			else
			{
				closest = 0;
				for (int j = 0; j < pNumHulls; j++)
				{
					Q_memset(&testtrace, 0, 0x44u);
					testtrace.endpos[0] = end[0];
					testtrace.endpos[1] = end[1];
					testtrace.endpos[2] = end[2];
					testtrace.fraction = 1.0f;
					testtrace.allsolid = 1;
					PM_RecursiveHullCheck(&hull[j], hull[j].firstclipnode, 0.0, 1.0, start_l, end_l, &testtrace);
					if (j == 0 || testtrace.allsolid || testtrace.startsolid || testtrace.fraction < total.fraction)
					{
						bool remember = (total.startsolid == 0);
						Q_memcpy(&total, &testtrace, sizeof(total));
						if (!remember)
							total.startsolid = 1;
						closest = j;
					}
					total.hitgroup = SV_HitgroupForStudioHull(closest);
				}
			}

			if (total.allsolid)
				total.startsolid = 1;

		}

		if (total.startsolid)
			total.fraction = 0;

		if (total.fraction != 1.0)
		{
			if (rotated)
			{
				vec3_t forward, right, up;
				AngleVectorsTranspose(pe->angles, forward, right, up);

				vec3_t temp = {total.plane.normal[0], total.plane.normal[1], total.plane.normal[2]};
				total.plane.normal[0] = _DotProduct(forward, temp);
				total.plane.normal[1] = _DotProduct(right, temp);
				total.plane.normal[2] = _DotProduct(up, temp);
			}
			total.endpos[0] = (end[0] - start[0]) * total.fraction + start[0];
			total.endpos[1] = (end[1] - start[1]) * total.fraction + start[1];
			total.endpos[2] = (end[2] - start[2]) * total.fraction + start[2];
		}

		if (total.fraction < trace.fraction)
		{
			Q_memcpy(&trace, &total, sizeof(trace));
			trace.ent = i;
		}
	}

	return trace;
}

/*EXT_FUNC*/ pmtrace_t PM_PlayerTrace(vec_t *start, vec_t *end, int traceFlags, int ignore_pe)
{
	pmtrace_t tr = _PM_PlayerTrace(start, end, traceFlags, pmove->numphysent, pmove->physents, ignore_pe, NULL);
	return tr;
};

/*EXT_FUNC*/ pmtrace_t PM_PlayerTraceEx(vec_t *start, vec_t *end, int traceFlags, int(*pfnIgnore)(physent_t *))
{
	pmtrace_t tr = _PM_PlayerTrace(start, end, traceFlags, pmove->numphysent, pmove->physents, -1, pfnIgnore);
	return tr;
};

/*EXT_FUNC*/ pmtrace_t *PM_TraceLine(float *start, float *end, int flags, int usehull, int ignore_pe)
{
	int oldhull;
	static pmtrace_t tr;

#ifndef SWDS
	g_engdstAddrs.PM_TraceLine(&start, &end, &flags, &usehull, &ignore_pe);
#endif

	oldhull = pmove->usehull;
	pmove->usehull = usehull;
	
	if (flags)
	{
		if (flags == 1)
			tr = _PM_PlayerTrace(start, end, PM_NORMAL, pmove->numvisent, pmove->visents, ignore_pe, nullptr);
	}
	else
		tr = _PM_PlayerTrace(start, end, PM_NORMAL, pmove->numphysent, pmove->physents, ignore_pe, nullptr);
	
	pmove->usehull = oldhull;
	return &tr;
};

/*EXT_FUNC*/ struct pmtrace_s *PM_TraceLineEx(float *start, float *end, int flags, int usehull, int(*pfnIgnore)(physent_t *))
{
	int oldhull;
	static pmtrace_t tr;

	oldhull = pmove->usehull;
	pmove->usehull = usehull;
	if (flags)
	{
		tr = _PM_PlayerTrace(start, end, PM_NORMAL, pmove->numvisent, pmove->visents, -1, pfnIgnore);
	}
	else
	{
		tr = PM_PlayerTraceEx(start, end, PM_NORMAL, pfnIgnore);
	}
	pmove->usehull = oldhull;
	return &tr;
};

/*
==================
PM_RecursiveHullCheck

==================
*/

// 1/32 epsilon to keep floating point happy
#define DIST_EPSILON (0.03125)

qboolean PM_RecursiveHullCheck(hull_t *hull, int num, float p1f, float p2f, vec3_t p1, vec3_t p2, pmtrace_t *trace)
{
	dclipnode_t *node;
	mplane_t *plane;
	float t1, t2;
	float frac;
	int i;
	vec3_t mid;
	int side;
	float midf;

	// check for empty
	if(num < 0)
	{
		if(num != CONTENTS_SOLID)
		{
			trace->allsolid = false;
			if(num == CONTENTS_EMPTY)
				trace->inopen = true;
			else
				trace->inwater = true;
		}
		else
			trace->startsolid = true;
		return true; // empty
	}

	if(num < hull->firstclipnode || num > hull->lastclipnode)
		Sys_Error("PM_RecursiveHullCheck: bad node number");

	//
	// find the point distances
	//
	node = hull->clipnodes + num;
	plane = hull->planes + node->planenum;

	if(plane->type < 3)
	{
		t1 = p1[plane->type] - plane->dist;
		t2 = p2[plane->type] - plane->dist;
	}
	else
	{
		t1 = DotProduct(plane->normal, p1) - plane->dist;
		t2 = DotProduct(plane->normal, p2) - plane->dist;
	}

#if 1
	if(t1 >= 0 && t2 >= 0)
		return PM_RecursiveHullCheck(hull, node->children[0], p1f, p2f, p1, p2, trace);
	if(t1 < 0 && t2 < 0)
		return PM_RecursiveHullCheck(hull, node->children[1], p1f, p2f, p1, p2, trace);
#else
	if((t1 >= DIST_EPSILON && t2 >= DIST_EPSILON) || (t2 > t1 && t1 >= 0))
		return PM_RecursiveHullCheck(hull, node->children[0], p1f, p2f, p1, p2, trace);
	if((t1 <= -DIST_EPSILON && t2 <= -DIST_EPSILON) || (t2 < t1 && t1 <= 0))
		return PM_RecursiveHullCheck(hull, node->children[1], p1f, p2f, p1, p2, trace);
#endif

	// put the crosspoint DIST_EPSILON pixels on the near side
	if(t1 < 0)
		frac = (t1 + DIST_EPSILON) / (t1 - t2);
	else
		frac = (t1 - DIST_EPSILON) / (t1 - t2);
	if(frac < 0)
		frac = 0;
	if(frac > 1)
		frac = 1;

	midf = p1f + (p2f - p1f) * frac;
	for(i = 0; i < 3; i++)
		mid[i] = p1[i] + frac * (p2[i] - p1[i]);

	side = (t1 < 0);

	// move up to the node
	if(!PM_RecursiveHullCheck(hull, node->children[side], p1f, midf, p1, mid, trace))
		return false;

#ifdef PARANOID
	if(PM_HullPointContents(pm_hullmodel, mid, node->children[side]) == CONTENTS_SOLID)
	{
		Con_Printf("mid PointInHullSolid\n");
		return false;
	}
#endif

	if(PM_HullPointContents(hull, node->children[side ^ 1], mid) != CONTENTS_SOLID)
		// go past the node
		return PM_RecursiveHullCheck(hull, node->children[side ^ 1], midf, p2f, mid, p2, trace);

	if(trace->allsolid)
		return false; // never got out of the solid area

	//==================
	// the other side of the node is solid, this is the impact point
	//==================
	if(!side)
	{
		VectorCopy(plane->normal, trace->plane.normal);
		trace->plane.dist = plane->dist;
	}
	else
	{
		VectorSubtract(vec3_origin, plane->normal, trace->plane.normal);
		trace->plane.dist = -plane->dist;
	}

	while(PM_HullPointContents(hull, hull->firstclipnode, mid) == CONTENTS_SOLID)
	{ // shouldn't really happen, but does occasionally
		frac -= 0.1;
		if(frac < 0)
		{
			trace->fraction = midf;
			VectorCopy(mid, trace->endpos);
			Con_DPrintf("backup past 0\n");
			return false;
		}
		midf = p1f + (p2f - p1f) * frac;
		for(i = 0; i < 3; i++)
			mid[i] = p1[i] + frac * (p2[i] - p1[i]);
	}

	trace->fraction = midf;
	VectorCopy(mid, trace->endpos);

	return false;
};