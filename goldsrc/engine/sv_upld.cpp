/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

//#include "precompiled.h"
#include "quakedef.h"
#include "hashpak.h"
#include "host.h"
#include "sv_upld.h"

// Checks MD5 of the resource against local cache and returns TRUE if resource was found or if downloads are disabled. Otherwise, if resource was requested from the player, it returns FALSE.
qboolean SV_CheckFile(sizebuf_t *msg, char *filename)
{
	resource_t p = { };

#ifdef REHLDS_FIXES

	// FIXED: First, check for allowed downloads, then try to lookup, this is faster if downloads aren't enabled
	if (sv_allow_upload.value == 0.0f)
	{
		// Downloads are not allowed, continue with the state we have
		return TRUE;
	}

	if (Q_strlen(filename) == 36 && Q_strnicmp(filename, "!MD5", 4) == 0)
	{
		// MD5 signature is correct, lets try to find this resource locally
		COM_HexConvert(filename + 4, 32, p.rgucMD5_hash);
		if (HPAK_GetDataPointer("custom.hpk", &p, 0, 0))
		{
			return TRUE;
		}
	}

#else // REHLDS_FIXES

	if (Q_strlen(filename) == 36 && Q_strnicmp(filename, "!MD5", 4) == 0)
	{
		// MD5 signature is correct, lets try to find this resource locally
		COM_HexConvert(filename + 4, 32, p.rgucMD5_hash);
		if (HPAK_GetDataPointer("custom.hpk", &p, 0, 0))
		{
			return TRUE;
		}
	}

	if (sv_allow_upload.value == 0.0f)
	{
		// Downloads are not allowed, continue with the state we have
		return TRUE;
	}

#endif // REHLDS_FIXES

#ifdef REHLDS_FIXES
	// While client is connecting he always send too small fragments (128 bytes)
	// But if client is fully connected he send fragments with cl_dlmax size
	// So, send upload in SV_SendEnts_f
	if (!sv_delayed_spray_upload.value)
#endif // REHLDS_FIXES
	{
		MSG_WriteByte(msg, svc_stufftext);
		MSG_WriteString(msg, va("upload \"!MD5%s\"\n", MD5_Print(p.rgucMD5_hash)));
	}

	return FALSE;
}

void SV_MoveToOnHandList(resource_t *pResource)
{
	if(!pResource)
	{
		Con_DPrintf("Null resource passed to SV_MoveToOnHandList\n");
		return;
	};
	
	SV_RemoveFromResourceList(pResource);
	SV_AddToResourceList(pResource, &host_client->resourcesonhand);
}

void SV_AddToResourceList(resource_t *pResource, resource_t *pList)
{
	if(pResource->pPrev || pResource->pNext)
	{
		Con_Printf("Resource already linked\n");
		return;
	};
	
	pResource->pPrev = pList->pPrev;
	pResource->pNext = pList;
	pList->pPrev->pNext = pResource;
	pList->pPrev = pResource;
}

void SV_RemoveFromResourceList(resource_t *pResource)
{
	pResource->pPrev->pNext = pResource->pNext;
	pResource->pNext->pPrev = pResource->pPrev;
	pResource->pPrev = nullptr;
	pResource->pNext = nullptr;
}

void SV_ClearResourceList(resource_t *pList)
{
	for(resource_t *pResource = pList->pNext, *pNext; pResource != pList; pResource = pNext)
	{
		pNext = pResource->pNext;
		SV_RemoveFromResourceList(pResource);

		Mem_Free(pResource);
	}

	pList->pPrev = pList;
	pList->pNext = pList;
}

void SV_ClearResourceLists(client_t *cl)
{
	if(!cl)
		Sys_Error("SV_ClearResourceLists with NULL client!");
		//Sys_Error("%s: SV_ClearResourceLists with NULL client!", __func__);

	SV_ClearResourceList(&cl->resourcesneeded);
	SV_ClearResourceList(&cl->resourcesonhand);
}

// For each t_decal and RES_CUSTOM resource the player had shown to us, tries to find it locally or count size required to be downloaded.
int SV_EstimateNeededResources()
{
	int iTotalSize = 0;

	for(auto i = host_client->resourcesneeded.pNext; i != &host_client->resourcesneeded; i = i->pNext)
	{
		if(i->type == t_decal)
		{
			if(!HPAK_ResourceForHash("custom.hpk", i->rgucMD5_hash, nullptr))
			{
				if(i->nDownloadSize)
				{
					i->ucFlags |= RES_WASMISSING;
					iTotalSize += i->nDownloadSize;
				}
			}
		}
	}

	return iTotalSize;
}

// Reinitializes customizations list. Tries to create customization for each resource in on-hands list.
void SV_CreateCustomizationList(client_t *pHost)
{
	int nLumps;
	customization_t *pCust;
	qboolean bIgnoreDup;

	for(auto pResource = pHost->resourcesonhand.pNext; pResource != &pHost->resourcesonhand;)
	{
		nLumps = 0;

		if(COM_CreateCustomization(&pHost->customdata, pResource, -1, RES_FATALIFMISSING | RES_WASMISSING, &pCust, &nLumps))
		{
			pCust->nUserData2 = nLumps;
			gEntityInterface.pfnPlayerCustomization(pHost->edict, pCust);
		}
		else
		{
			if(!sv_allow_upload.value)
			{
				Con_Printf("Ignoring custom decal from %s\n", pHost->name);
			}
			else
			{
				Con_Printf("Ignoring invalid custom decal from %s\n", pHost->name);
			}
		}

		pResource = pResource->pNext;

		bIgnoreDup = false;

		//Check if the next resource is a duplicate
		while(pResource)
		{
			for(auto pCustom = pHost->customdata.pNext;
			    pCustom;
			    pCustom = pCustom->pNext)
			{
				if(!Q_memcmp(pCustom->resource.rgucMD5_hash, pResource->rgucMD5_hash, sizeof(pResource->rgucMD5_hash)))
				{
					Con_DPrintf("SV_CreateCustomization list, ignoring dup. resource for player %s\n", pHost->name);
					bIgnoreDup = true;
					break;
				}
			}

			if(!bIgnoreDup)
				break;

			pResource = pResource->pNext;
		}
	}
}

// Creates customizations list for the current player and sends resources to other players.
void SV_RegisterResources()
{
	host_client->uploading = false;

#ifdef REHLDS_FIXES
	SV_CreateCustomizationList(host_client);		// FIXED: Call this function only once. It was crazy to call it for each resource available.
	for(auto pResource = host_client->resourcesonhand.pNext; pResource != &host_client->resourcesonhand; pResource = pResource->pNext)
	{
		SV_Customization(host_client, pResource, TRUE);
	}
#else // REHLDS_FIXES
	for(auto pResource = host_client->resourcesonhand.pNext; pResource != &host_client->resourcesonhand; pResource = pResource->pNext)
	{
		SV_CreateCustomizationList(host_client);
		SV_Customization(host_client, pResource, true);
	}
#endif // REHLDS_FIXES
}

// Creates customizations list for a player (the current player actually, see the caller) 
// and sends them out to other players when all needed resources are on-hands
// Also sends other players customizations to the current player
qboolean SV_UploadComplete(client_t *cl)
{
	if(cl->resourcesneeded.pNext == &cl->resourcesneeded)
	{
		// All resources are available locally, now we can do customizations propagation
		SV_RegisterResources();

		SV_PropagateCustomizations();

		if(sv_allow_upload.value)
			Con_DPrintf("Custom resource propagation complete.\n");

		cl->uploaddoneregistering = true;

		return true;
	}

	return false;
}

// For each resource the player had shown to us, moves it to on-hands list. For t_decal and RES_CUSTOM it tries to find it locally or request resource from the player.
void SV_BatchUploadRequest(client_t *cl)
{
	resource_t *p, *n;
	char filename[MAX_PATH];

	for( p = cl->resourcesneeded.pNext; p != &cl->resourcesneeded; p = n )
	{
		n = p->pNext;

		if ((p->ucFlags & RES_WASMISSING) == 0)
		{
			SV_MoveToOnHandList(p);
		}
		else if (p->type == t_decal)
		{
			if (p->ucFlags & RES_CUSTOM)
			{
				Q_snprintf(filename, sizeof(filename), "!MD5%s", MD5_Print(p->rgucMD5_hash));
				if (SV_CheckFile(&cl->netchan.message, filename))
				{
					SV_MoveToOnHandList(p);
				}
			}
			else
			{
				Con_Printf("Non customization in upload queue!\n");
				SV_MoveToOnHandList(p);
			}
		}
	}
}

// This is used to do recurring checks on the current player that he uploaded all resources that where needed.
qboolean SV_RequestMissingResources()
{
	if(host_client->uploading && !host_client->uploaddoneregistering)
		SV_UploadComplete(host_client); // TODO: return?

	return false;
}

void SV_RequestMissingResourcesFromClients()
{
	host_client = svs.clients;

	for(int i = 0; i < svs.maxclients; ++i, ++host_client)
		SV_RequestMissingResources();
}

// Sends resource to all other players, optionally skipping originating player.
void SV_Customization(client_t *pPlayer, resource_t *pResource, qboolean bSkipPlayer)
{
	int i;

	host_client = svs.clients;

	// Get originating player id
	for(i = 0; i < svs.maxclients; ++i, ++host_client)
	{
		if(host_client == pPlayer)
			break;
	}

	if(i == svs.maxclients)
		Sys_Error("Couldn't find player index for customization.");
		//Sys_Error("%s: Couldn't find player index for customization.", __func__);

	for(int cl = 0; cl < svs.maxclients; ++cl)
	{
		if((host_client->active || host_client->spawned) &&
		   !host_client->fakeclient &&
		   (pPlayer != host_client || !bSkipPlayer))
		{
			MSG_WriteByte(&host_client->netchan.message, 46);
			MSG_WriteByte(&host_client->netchan.message, i);
			MSG_WriteByte(&host_client->netchan.message, pResource->type);
			MSG_WriteString(&host_client->netchan.message, pResource->szFileName);
			MSG_WriteShort(&host_client->netchan.message, pResource->nIndex);
			MSG_WriteLong(&host_client->netchan.message, pResource->nDownloadSize);
			MSG_WriteByte(&host_client->netchan.message, pResource->ucFlags);

			if(pResource->ucFlags & RES_CUSTOM)
				SZ_Write(&host_client->netchan.message, pResource->rgucMD5_hash, 16);
		}
	}
}