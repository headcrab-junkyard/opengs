/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/// @file
/// @brief player eye positioning

#include "quakedef.h"
#include "r_local.h"
//#include "server.h"
//#include "view.h"

/*

The view is allowed to move slightly from it's true position for bobbing,
but if it exceeds 8 pixels linear distance (spherical, not box), the list of
entities sent from the server may not include everything in the pvs, especially
when crossing a water boudnary.

*/

cvar_t crosshair = { "crosshair", "0", true }; // Enables the engine default crosshair

//cvar_t v_dark = {"v_dark", "0"} // Setting this to 1 causes the level to fade in from black upon initial load.

float v_dmg_time, v_dmg_roll, v_dmg_pitch;

//=============================================================================

cvar_t v_gamma = { "gamma", "1", true };

byte gammatable[256]; // palette is sent through this

#ifdef GLQUAKE
byte ramps[3][256];
float v_blend[4]; // rgba 0.0 - 1.0
#endif            // GLQUAKE

void BuildGammaTable(float g)
{
	int i, inf;

	if(g == 1.0)
	{
		for(i = 0; i < 256; i++)
			gammatable[i] = i;
		return;
	}

	for(i = 0; i < 256; i++)
	{
		inf = 255 * pow((i + 0.5) / 255.5, g) + 0.5;
		if(inf < 0)
			inf = 0;
		if(inf > 255)
			inf = 255;
		gammatable[i] = inf;
	}
}

/*
=================
V_CheckGamma
=================
*/
qboolean V_CheckGamma()
{
	static float oldgammavalue;

	if(v_gamma.value == oldgammavalue)
		return false;
	oldgammavalue = v_gamma.value;

	BuildGammaTable(v_gamma.value);
	vid.recalc_refdef = 1; // force a surface cache flush

	return true;
}

/*
===============
V_ParseDamage
===============
*/
void V_ParseDamage()
{
	int armor, blood;
	vec3_t from;
	int i;
	vec3_t forward, right, up;
	cl_entity_t *ent;
	float side;
	float count;

	armor = MSG_ReadByte();
	blood = MSG_ReadByte();
	for(i = 0; i < 3; i++)
		from[i] = MSG_ReadCoord();

	count = blood * 0.5 + armor * 0.5;
	if(count < 10)
		count = 10;

	cl.faceanimtime = cl.time + 0.2; // but sbar face into pain frame

	//
	// calculate view angle kicks
	//
	ent = &cl_entities[cl.viewentity];

	//VectorSubtract (from, cl.simorg, from);
	VectorSubtract(from, ent->origin, from);
	VectorNormalize(from);

	//AngleVectors (cl.simangles, forward, right, up);
	AngleVectors(ent->angles, forward, right, up);

	side = DotProduct(from, right);
	v_dmg_roll = count * side * 0;

	side = DotProduct(from, forward);
	v_dmg_pitch = count * side * 0;

	v_dmg_time = 0;
}

/*
=============
V_UpdatePalette
=============
*/
#ifdef GLQUAKE
void V_UpdatePalette()
{
	int i, j;
	qboolean bnew;
	byte *basepal, *newpal;
	byte pal[768];
	float r, g, b, a;
	int ir, ig, ib;
	qboolean force;

	bnew = false;

	force = V_CheckGamma();
	if(!bnew && !force)
		return;

	//Con_Printf("b: %4.2f %4.2f %4.2f %4.6f\n", v_blend[0],	v_blend[1],	v_blend[2],	v_blend[3]);

	a = v_blend[3];
	r = 255 * v_blend[0] * a;
	g = 255 * v_blend[1] * a;
	b = 255 * v_blend[2] * a;

	a = 1 - a;
	for(i = 0; i < 256; i++)
	{
		ir = i * a + r;
		ig = i * a + g;
		ib = i * a + b;
		if(ir > 255)
			ir = 255;
		if(ig > 255)
			ig = 255;
		if(ib > 255)
			ib = 255;

		ramps[0][i] = gammatable[ir];
		ramps[1][i] = gammatable[ig];
		ramps[2][i] = gammatable[ib];
	}

	basepal = host_basepal;
	newpal = pal;

	for(i = 0; i < 256; i++)
	{
		ir = basepal[0];
		ig = basepal[1];
		ib = basepal[2];
		basepal += 3;

		newpal[0] = ramps[0][ir];
		newpal[1] = ramps[1][ig];
		newpal[2] = ramps[2][ib];
		newpal += 3;
	}

	VID_ShiftPalette(pal);
}
#else  // !GLQUAKE
void V_UpdatePalette()
{
	int i, j;
	qboolean bnew;
	byte *basepal, *newpal;
	byte pal[768];
	int r, g, b;
	qboolean force;

	bnew = false;

	force = V_CheckGamma();
	if(!bnew && !force)
		return;

	basepal = host_basepal;
	newpal = pal;

	for(i = 0; i < 256; i++)
	{
		r = basepal[0];
		g = basepal[1];
		b = basepal[2];
		basepal += 3;

		newpal[0] = gammatable[r];
		newpal[1] = gammatable[g];
		newpal[2] = gammatable[b];
		newpal += 3;
	}

	VID_ShiftPalette(pal);
}
#endif // !GLQUAKE

/* 
============================================================================== 
 
						VIEW RENDERING 
 
============================================================================== 
*/

float angledelta(float a)
{
	a = anglemod(a);
	if(a > 180)
		a -= 360;
	return a;
}

/*
==============
V_BoundOffsets
==============
*/
void V_BoundOffsets()
{
	cl_entity_t *ent = &cl_entities[cl.viewentity];

	// absolutely bound refresh relative to entity clipping hull
	// so the view can never be inside a solid wall

	//
	if(r_refdef.vieworg[0] < ent->origin[0] - 14)
		r_refdef.vieworg[0] = ent->origin[0] - 14;
	else if(r_refdef.vieworg[0] > ent->origin[0] + 14)
		r_refdef.vieworg[0] = ent->origin[0] + 14;
	if(r_refdef.vieworg[1] < ent->origin[1] - 14)
		r_refdef.vieworg[1] = ent->origin[1] - 14;
	else if(r_refdef.vieworg[1] > ent->origin[1] + 14)
		r_refdef.vieworg[1] = ent->origin[1] + 14;
	if(r_refdef.vieworg[2] < ent->origin[2] - 22)
		r_refdef.vieworg[2] = ent->origin[2] - 22;
	else if(r_refdef.vieworg[2] > ent->origin[2] + 30)
		r_refdef.vieworg[2] = ent->origin[2] + 30;
	//
	// qw
	/*
	if (r_refdef.vieworg[0] < cl.simorg[0] - 14)
		r_refdef.vieworg[0] = cl.simorg[0] - 14;
	else if (r_refdef.vieworg[0] > cl.simorg[0] + 14)
		r_refdef.vieworg[0] = cl.simorg[0] + 14;
	if (r_refdef.vieworg[1] < cl.simorg[1] - 14)
		r_refdef.vieworg[1] = cl.simorg[1] - 14;
	else if (r_refdef.vieworg[1] > cl.simorg[1] + 14)
		r_refdef.vieworg[1] = cl.simorg[1] + 14;
	if (r_refdef.vieworg[2] < cl.simorg[2] - 22)
		r_refdef.vieworg[2] = cl.simorg[2] - 22;
	else if (r_refdef.vieworg[2] > cl.simorg[2] + 30)
		r_refdef.vieworg[2] = cl.simorg[2] + 30;
	*/
	//
}

void V_CalcRefdef()
{
	vec3_t forward, right, up;

	// transform the view offset by the model's matrix to get the offset from
	// model origin for the view
	ent->angles[YAW] = cl.viewangles[YAW];      // the model should face
	                                            // the view dir
	ent->angles[PITCH] = -cl.viewangles[PITCH]; // the model should face
	                                            // the view dir

	bob = V_CalcBob();

	// refresh position
	VectorCopy(ent->origin, r_refdef.vieworg);
	r_refdef.vieworg[2] += cl.viewheight + bob;

	// never let it sit exactly on a node line, because a water plane can
	// dissapear when viewed with the eye exactly on it.
	// the server protocol only specifies to 1/16 pixel, so add 1/32 in each axis
	r_refdef.vieworg[0] += 1.0 / 32;
	r_refdef.vieworg[1] += 1.0 / 32;
	r_refdef.vieworg[2] += 1.0 / 32;

	VectorCopy(cl.viewangles, r_refdef.viewangles);
	V_CalcViewRoll();
	V_AddIdle();

	// offsets
	angles[PITCH] = -ent->angles[PITCH]; // because entity pitches are
	                                     //  actually backward
	angles[YAW] = ent->angles[YAW];
	angles[ROLL] = ent->angles[ROLL];

	AngleVectors(angles, forward, right, up);

	for(i = 0; i < 3; i++)
		r_refdef.vieworg[i] += scr_ofsx.value * forward[i] + scr_ofsy.value * right[i] + scr_ofsz.value * up[i];

	V_BoundOffsets();

	// set up gun position
	VectorCopy(cl.viewangles, view->angles);

	CalcGunAngle();

	VectorCopy(ent->origin, view->origin);
	view->origin[2] += cl.viewheight;

	for(i = 0; i < 3; i++)
	{
		view->origin[i] += forward[i] * bob * 0.4;
		//		view->origin[i] += right[i]*bob*0.4;
		//		view->origin[i] += up[i]*bob*0.8;
	}
	view->origin[2] += bob;

// fudge position around to keep amount of weapon visible
// roughly equal with different FOV

#if 0
	if (cl.model_precache[cl.stats[STAT_WEAPON]] && strcmp (cl.model_precache[cl.stats[STAT_WEAPON]]->name,  "progs/v_shot2.mdl"))
#endif

	if(scr_viewsize.value == 110)
		view->origin[2] += 1;
	else if(scr_viewsize.value == 100)
		view->origin[2] += 2;
	else if(scr_viewsize.value == 90)
		view->origin[2] += 1;
	else if(scr_viewsize.value == 80)
		view->origin[2] += 0.5;

	view->model = cl.model_precache[cl.stats[STAT_WEAPON]];
	view->frame = cl.stats[STAT_WEAPONFRAME];
	view->colormap = vid.colormap;

	// set up the refresh position
	VectorAdd(r_refdef.viewangles, cl.punchangle, r_refdef.viewangles);

	// smooth out stair step ups
	if(cl.onground && ent->origin[2] - oldz > 0)
	{
		float steptime;

		steptime = cl.time - cl.oldtime;
		if(steptime < 0)
			//FIXME		I_Error ("steptime < 0");
			steptime = 0;

		oldz += steptime * 80;
		if(oldz > ent->origin[2])
			oldz = ent->origin[2];
		if(ent->origin[2] - oldz > 12)
			oldz = ent->origin[2] - 12;
		r_refdef.vieworg[2] += oldz - ent->origin[2];
		view->origin[2] += oldz - ent->origin[2];
	}
	else
		oldz = ent->origin[2];

	if(chase_active.value)
		Chase_Update();
}

/*
==================
V_RenderView

The player's clipping box goes from (-16 -16 -24) to (16 16 32) from
the entity origin, so any view position inside that will be valid
==================
*/
extern vrect_t scr_vrect;

void V_RenderView()
{
	//	if (cl.simangles[ROLL])
	//		Sys_Error ("cl.simangles[ROLL]");	// DEBUG
	//cl.simangles[ROLL] = 0;	// FIXME @@@

	//if (cls.state != ca_active)
	//return;

	if(con_forcedup)
		return;

	// don't allow cheats in multiplayer
	if(cl.maxclients > 1)
	{
		Cvar_Set("scr_ofsx", "0");
		Cvar_Set("scr_ofsy", "0");
		Cvar_Set("scr_ofsz", "0");
	}

	//if (!cl.paused /* && (sv.maxclients > 1 || key_dest == key_game) */ )
	V_CalcRefdef(); // -> ClientDLL_CalcRefDef(refdef);

	R_PushDlights();
	R_RenderView();

#ifndef GLQUAKE
	if(crosshair.value)
		Draw_Crosshair();
#endif
}

//============================================================================

/*
=============
V_Init
=============
*/
void V_Init()
{
	Cvar_RegisterVariable(&crosshair);

	BuildGammaTable(1.0); // no gamma yet

	Cvar_RegisterVariable(&v_gamma);
}

void V_CalcShake(){
	//TODO: implement - Solokiller
};

void V_ApplyShake(float *origin, float *angles, float factor){
	//TODO: implement - Solokiller
};

int V_ScreenShake(const char *pszName, int iSize, void *pbuf)
{
	//TODO: implement - Solokiller
	return 0;
};

int V_ScreenFade(const char *pszName, int iSize, void *pbuf)
{
	//TODO: implement - Solokiller
	return 0;
};