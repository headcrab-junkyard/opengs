/// @file

#include "quakedef.h"
#include "ogs_ext.h"

bool OGS_Init()
{
#ifdef OGS_INTEGRATE_METAMOD
	if(!metamod_init())
		return false;
#endif

#ifdef OGS_INTEGRATE_METAHOOK
	if(!MH_Init())
		return false;
#endif

	return true;
};

void OGS_Shutdown()
{
#ifdef OGS_INTEGRATE_METAMOD
	metamod_shutdown();
#endif

#ifdef OGS_INTEGRATE_METAHOOK
	MH_Shutdown();
#endif
};