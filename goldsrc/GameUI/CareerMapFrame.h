/// @file

#pragma once

#include <Color.h> //#include <SDK_Color.h>
#include <vgui_controls/RichText.h>

class CCareerMap;

void ShowCareerMapDescription(vgui2::RichText *pText, CCareerMap *pMap, SDK_Color normalColor, SDK_Color friendlyFireColor);