#
#	FileSystem library
#	Shared library providing the SteamPipe filesystem. Implements the GoldSource IFileSystem interface.
#

# TODO: stringutils.cpp should be removed. - Solokiller
#add_sources
set(PROJECT_SOURCES
	${SRC_DIR}/common/StringUtils.cpp
	#${SRC_DIR}/public/characterset.cpp
	#${OGS_HLSDK_PATH}/public/interface.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/CFileHandle.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/CFileSystem.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/CFileSystem.obsolete.cpp
)

add_library(${PROJECT_NAME} MODULE ${PROJECT_SOURCES})

# TODO: common is not used by non-temporary code - Solokiller
target_include_directories( ${PROJECT_NAME} PRIVATE
	${SRC_DIR}/common
	${SRC_DIR}/public
	${SRC_DIR}/public/tier1
	${SHARED_INCLUDE_PATHS}
	${SHARED_EXTERNAL_INCLUDE_PATHS}
)

target_compile_definitions( ${PROJECT_NAME} PRIVATE
	${SHARED_DEFS}
)

# Link with dependencies
target_link_libraries( ${PROJECT_NAME} 
	${UNIX_FS_LIB}
)

#CMake places libraries in /Debug or /Release on Windows, so explicitly set the paths for both.
#On Linux, it uses LIBRARY_OUTPUT_DIRECTORY
set_target_properties( ${PROJECT_NAME} PROPERTIES
	LIBRARY_OUTPUT_DIRECTORY "${ENGINE_BASE_PATH}"
	RUNTIME_OUTPUT_DIRECTORY_DEBUG "${ENGINE_BASE_PATH}"
	RUNTIME_OUTPUT_DIRECTORY_RELEASE "${ENGINE_BASE_PATH}"
)

set( PROJECT_LINK_FLAGS )

if( NOT MSVC AND NOT APPLE )
	#Generate a map file that provides information about the linking stage.
	set( PROJECT_LINK_FLAGS
		${PROJECT_LINK_FLAGS} "-Wl,-Map,${PROJECT_NAME}_map.txt "
	)
endif()

set_target_properties( ${PROJECT_NAME} PROPERTIES
	COMPILE_FLAGS "${LINUX_32BIT_FLAG}"
	LINK_FLAGS "${SHARED_ENGINE_LINKER_FLAGS} ${PROJECT_LINK_FLAGS} ${LINUX_32BIT_FLAG}"
)

set_target_properties(${PROJECT_NAME} PROPERTIES
	OUTPUT_NAME "filesystem_stdio"
)

target_link_libraries(${PROJECT_NAME} -lstdc++fs)

#if(CLANG)
#	target_link_libraries(${PROJECT_NAME} -lstdc++experimental)
#endif()

target_link_libraries(${PROJECT_NAME} GoldSrc-tier1)